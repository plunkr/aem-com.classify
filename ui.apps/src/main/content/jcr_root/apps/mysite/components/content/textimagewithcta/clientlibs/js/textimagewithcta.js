(function($, ns, channel, window, undefined) {
	"use strict";
	var RESOURCE_TYPE = 'classify/components/content/textimagewithcta';
	var ERROR_MSG = "An error occurred while processing/fetching the response";
	var SUCCESS_MSG = "Successfully fetched the response";

	var getRecommendedTextToolbarAction = new ns.ui.ToolbarAction({
		name : 'RECOMMENDED_TEXT',
		text : Granite.I18n.get('Recommended Text'),
		icon : 'adobeMediaOptimizer',
		order : 'before COPY',
		execute : function(editable, param, target) {
			var windowURLVal = window.location.pathname;
			var aemPagePath = windowURLVal.split(".html")[1];
			$.ajax({
				type : 'GET',
				url : '/travel/reco/pkg.GET_HOLIDAY_DATA.json',
				data : {
					contentPath : editable.path,
					pagePath : aemPagePath
				},
				success : function(data) {
					if (data != "" || data != undefined) {
						var jsonObj = JSON.parse(data);
						console.log(jsonObj);
						if (jsonObj != undefined && jsonObj.Success == true) {

							if (jsonObj.CtaLabel != undefined) {
								var ctaLabel = jsonObj.CtaLabel[0];
								$.ajax({
									type : 'POST',
									url : editable.path,
									data : {
										// 'alt' : alttext,
										'buttontext' : ctaLabel

									}
								})

							}
						} else {
							$(window).adaptTo("foundation-ui").alert("ERROR",
									ERROR_MSG, "error")
						}
					} else {
						$(window).adaptTo("foundation-ui").alert("ERROR",
								ERROR_MSG, "error")
					}

				},
				error : function() {
					$(window).adaptTo("foundation-ui").alert("ERROR",
							ERROR_MSG, "error")
				}
			});
		},
		condition : function(editable) {
			return editable.type === RESOURCE_TYPE;
		},
		isNonMulti : true
	});

	channel.on("cq-layer-activated", function(event) {
		if (event.layer === "Edit") {
			ns.EditorFrame.editableToolbar.registerAction("RECOMMENDED_TEXT",
					getRecommendedTextToolbarAction)
		}
	});

}(jQuery, Granite.author, jQuery(document), this));