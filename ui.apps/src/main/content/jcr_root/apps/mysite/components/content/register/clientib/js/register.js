    function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false) 
        {
             errormessage('Invalid Email address');
            return false;
        }
        else{
             var x = document.getElementById("error");
             x.style.display = "none";
        }

        return true;

}
 function validatePhone(mobileno){
        var reg1 = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;

        if (reg1.test(mobileno.value) == false) 
        {
             errormessage('Invalid Phonenumber');
            return false;
        }
        else{
             var x = document.getElementById("error");
             x.style.display = "none";
        }

        return true;

}



function createUUID() {

    var x = Math.floor((Math.random() * 1000000) + 1);

    var uuid = x;
    return uuid;
}

function errormessage(emsg) {

    var x = document.getElementById("changer");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
    var sucessmsg1 = emsg;
    $("#changer").html(sucessmsg1);

}
var getRedirectPath = function() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var value = url.searchParams.get("src");
    if (!value) {
        return "travel-home-page-template.html";
    } else {
        return value;
    }
}


$(document).ready(function() {

    $('body').hide().fadeIn(5000);



    $('#submit').click(function() {
        var failure = function(err) {
            alert("Unable to retrive data " + err);
        };
        var myFirst = $('#FirstName').val();
        var myLast = $('#LastName').val();
        var signUpmobileno = $('#signUpmobileno').val();
        var uId = createUUID();
        var Email = $('#Email').val();
        var Password = $('#Password').val();
        var cPassword = $('#signUpConfirmpswd').val();

        if (!Email || !signUpmobileno ||  !myFirst) {
            errormessage('Email/Mobile/First Name  is mandatory ');
            return false;
        }

        if ((cPassword !== Password)) {
            var x = document.getElementById("changer");
            if (x.style.display === "none") {
                x.style.display = "block";
            }
            var sucessmsg1 = ' Password and confirmation password do not match ';
            $("#changer").html(sucessmsg1);
            return;
        }
        //Use JQuery AJAX request to post data to a Sling Servlet
        $.ajax({
            type: 'POST',
            url: '/bin/myRegisterServlet',
            data: 'Email=' + Email + '&Password=' + Password + '&id=' + uId + '&firstName=' + myFirst + '&lastName=' + myLast + '&signUpmobileno=' + signUpmobileno,
            success: function(msg) {
                $('#changer').attr("class", "alert alert-success");

                var x = document.getElementById("changer");
                if (x.style.display === "none") {
                    x.style.display = "block";
                }
                var sucessmsg = 'Your account has been created successfully and Username:' + msg;
                $("#changer").html(sucessmsg);
                window.location.href = getRedirectPath();
            },
            error: function(jqXHR, textStatus, message) {
               
                var response = $.parseHTML(jqXHR.responseText);
                if (jqXHR.status == 400) {
                    var x = document.getElementById("changer");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    }
                    $("#changer").html($(response).filter('h1').text());
                }
            }
        });
    });

}); // end ready