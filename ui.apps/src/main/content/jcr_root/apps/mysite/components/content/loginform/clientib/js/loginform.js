var getRedirectPath = function() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var value = url.searchParams.get("src");

    if(value == 'flight-listing-page.html')
    {
	var travelclass = url.searchParams.get("travelclass"); 
	var date = url.searchParams.get("date"); 
	var passengernum = url.searchParams.get("passengernum");
	var from= url.searchParams.get("from");
	var to= url.searchParams.get("to");
	var returndate= url.searchParams.get("returndate");
	var checkbox= url.searchParams.get("checkbox");  

        return 'flight-listing-page.html?'+'&travelclass='+travelclass+'&date='+date+'&passengernum='+passengernum+'&from='+from+'&to='+to+'&returndate='+returndate+'&checkbox='+checkbox;
    }

    if (!value) {
        return "travel-home-page-template.html";
    } else {
        return value;
    }
}

function errormessage(emsg) {

    var x = document.getElementById("changer");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
    var sucessmsg1 = emsg;
    $("#changer").html(sucessmsg1);

}


$(document).ready(function() {

    $('#submit').click(function() {
        // alert('enter submit');
        var failure = function(err) {
            alert("Unable to retrive data " + err);
        };


        var password = $('#inputPassword').val();
        var Email = $('#userId').val();

     
        if (!Email || !password ) {
          
             errormessage('Email and Password is required ');
            return;
        }

       
        $.ajax({
            type: 'POST',
            url: '/bin/myLoginformServlet',
            data: 'Email=' + Email + '&password=' + password,
            success: function(msg) {

               // setCookie("mtuser_c", Email, 10);

                $('#json').val("User Created With ID =" + msg);
                window.location.href = getRedirectPath();
            },
            error: function(jqXHR, textStatus, message) {
                
                var response = $.parseHTML(jqXHR.responseText);
                if (jqXHR.status == 400) {
                   
                    var x = document.getElementById("changer");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    }
                    $("#changer").html($(response).filter('h1').text());

                }
            }
        });
    });

}); // end ready