(function($, ns, channel, window, undefined) {
	"use strict";
    var RESOURCE_TYPE = 'classify/components/content/recommendedtitle'

	var getRecommendedTextToolbarAction = new ns.ui.ToolbarAction(
			{
				name : 'RECOMMENDED_TEXT',
				text : Granite.I18n.get('Recommended Text'),
				icon : 'adobeMediaOptimizer',
				order : 'before COPY',
				execute : function(editable, param, target) {
					alert('Executed!'+this.item)
					//alert('Executed!***' + editable.path)
					$.ajax({
						type : 'GET',
						url : '/travel/reco/pkg.GET_HOLIDAY_DATA.now121',
                        data : {
                            input: "my input"
                        },
						success : function(data) {

							//alert('success*******'+msg)
							//alert(editable.dom.text(msg))

							//tagdata = mydata;
							console.log(data);
							if (data != undefined && data.Success == true) {

								if (data.Packages != undefined) {
									var packages = data.Packages;
									alert('Success***!' + packages)
									editable.dom.text(packages)
									$.ajax({
										type : 'POST',
										url : editable.path,
										data : {
											'jcr:title' : packages
										}
									})
									
								}
							}

						}
					});
				},
				condition : function(editable) {
					return editable.type === RESOURCE_TYPE;
				},
				isNonMulti : true
			});


	channel.on("cq-layer-activated", function(event) {
		if (event.layer === "Edit") {
			ns.EditorFrame.editableToolbar.registerAction("RECOMMENDED_TEXT",
					getRecommendedTextToolbarAction)
		}
	});

}(jQuery, Granite.author, jQuery(document), this));