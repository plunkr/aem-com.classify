
 $(document).ready(function() {
	 if($('.holiPackdetails').length >0){
		 
		 if(!sessionStorage.getItem("HolidayList")){
		     	$.ajax({
		         	url: "/etc/clientlibs/HolidayPackage.json", 
		         	success: function(result){
		         		var packageslist=result.packageDetails;
		         		sessionStorage.setItem("HolidayList",JSON.stringify(packageslist));
		         		var id = getUrlParameter("id");
		         		fetchPackageDetails(pakageslist,id);
		         		
		         }});
		     }else{
		     	var packageslistvalue=sessionStorage.getItem("HolidayList");
		     	var pakageslist=JSON.parse(packageslistvalue);
		     	var id = getUrlParameter("id");
		     	fetchPackageDetails(pakageslist,id);
		     }
			 
			 
		 function getUrlParameter(sParam) {
		    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		        sURLVariables = sPageURL.split('&'),sParameterName,i;
		    for (i = 0; i < sURLVariables.length; i++) {
		        sParameterName = sURLVariables[i].split('=');
		        if (sParameterName[0] === sParam) {
		            return sParameterName[1] === undefined ? true : sParameterName[1];
		        }
		    }
		};

		 function fetchPackageDetails(packageList,id){
			 var imgSliderHtml=""
		   if(id){
			    for(var i in packageList){
				     var idValue = packageList[i].id; 
				     if(idValue==id){
				    	 var imagesList= packageList[i].imageDetails.images;
				    	 for(var j in imagesList){
				    		 imgSliderHtml =imgSliderHtml+'<li><img src="'+imagesList[j].path+'" /><div class="slider-text-inner text-center">';
							 imgSliderHtml =imgSliderHtml+'<p class="flex-caption">'+imagesList[j].title+'</p></div></li>';					  
				    	 }
				    	 
				    	 //code for itinerary
				    	 var dayActivitylist=packageList[i].activityDetails;
				    	 itineraryData(dayActivitylist);
				    	 
				    	 //code for faq
				    	 var faqlist=packageList[i].faq;
				    	 faq(faqlist);
				     }
		        }
			   // $('.cust-mainSlider').html(imgSliderHtml);
		      }
			}
		 
		 function itineraryData(dayActivitylist){
			 var itineraryDom="";
			 $('#holiItinerary').html("");
			 var count=1;
			 for(var k in dayActivitylist){
				 itineraryDom=itineraryDom+'<div class="days-info"><h5><a href="#">';
			     itineraryDom=itineraryDom+'<span class="bullet">'+count+'</span>'+dayActivitylist[k].title+'<i class="fa fa-chevron-down pull-right"></i>';
			     itineraryDom=itineraryDom+'<i class="fa fa-chevron-up pull-right"></i></a></h5>';
			     itineraryDom=itineraryDom+'<div class="day-con" style="display: block;"><p>'+dayActivitylist[k].Desc+'</p></div></div>';
			     count++;
			 }
			 $('#holiItinerary').html(itineraryDom);
		 }
		 function faq(faqlist){
			 var faqDom="";
			 $('.accordion_container').html("");
			 for(var k in faqlist){
				 faqDom=faqDom+'<div class="accordion_head">General Questions<span class="plusminus pull-left">+</span></div>';
				 faqDom=faqDom+'<div class="accordion_body" style="display: block;">';
				 faqDom=faqDom+'<p>'+faqlist[k].que+'</p>';
				 faqDom=faqDom+'<p class="faq-ans">'+faqlist[k].ans+'</p></div>';
				
			 }
			 $('.accordion_container').html(faqDom);
		 }
		 
		 
	 }
});