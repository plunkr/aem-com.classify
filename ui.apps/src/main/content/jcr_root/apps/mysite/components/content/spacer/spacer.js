"use strict";
use(function() {

    var styleString = '';

    if(properties.size === 'size-custom' && properties.sizePx) {
        if(properties.lineDirection === 'line-vertical') {
            styleString += ' height: ' + properties.sizePx + 'px;';
        } else {
            var sizePx = properties.sizePx / 2;
            styleString += ' margin: ' + sizePx + 'px 0;';
        }
    }

    if(properties.lineWidth) {
        styleString += ' width: ' + properties.lineWidth;
    }

    return {
        style: styleString
    }
});
