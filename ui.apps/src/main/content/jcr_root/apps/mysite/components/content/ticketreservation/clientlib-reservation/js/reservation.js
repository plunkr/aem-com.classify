

var currentTab = 0; 
	showTab(currentTab); 

	function showTab(n) {

		var x = document.getElementsByClassName("tab");
       		x[n].style.display = "block";
		if (n == 0) {
			document.getElementById("prevBtn").style.display = "none";
		} else {
			document.getElementById("prevBtn").style.display = "inline";
		}
		if (n == (x.length - 1)) {
			document.getElementById("nextBtn").innerHTML = "Submit";
			document.getElementById("nextBtn").setAttribute('onclick','onSubmit()');
		} else {
			document.getElementById("nextBtn").innerHTML = "Continue";
		}
		fixStepIndicator(n);
	}

	function nextPrev(n) {
		var x = document.getElementsByClassName("tab");
		if (n == 1 && !validateForm()) return false;
		x[currentTab].style.display = "none";
		currentTab = currentTab + n;
		if (currentTab >= x.length) {
			document.getElementById("regForm").submit();
			return false;
		}
		showTab(currentTab);
	}

	function validateForm() {
		var x, y, i, valid = true;
		x = document.getElementsByClassName("tab");
		y = x[currentTab].getElementsByTagName("input");
		for (i = 0; i < y.length; i++) {
			if (y[i].value == "") {
				y[i].className += " invalid";
				valid = false;
			}
		}
		if (valid) {
			document.getElementsByClassName("step")[currentTab].className += " finish";
		}
		return valid;
	}

	function fixStepIndicator(n) {
		var i, x = document.getElementsByClassName("step");
		for (i = 0; i < x.length; i++) {
			x[i].className = x[i].className.replace(" active", "");
		}
		x[n].className += " active";
	}

	function onSubmit() {
        var url_string = window.location.href; 
var url = new URL(url_string);
var Fname=url.searchParams.get("Fname");
var Fid=url.searchParams.get("Fid");
var Dtime=url.searchParams.get("Dtime");
var Ffrom=url.searchParams.get("Ffrom");
var Atime=url.searchParams.get("Atime");
var Fto=url.searchParams.get("Fto");
var Ftravel=url.searchParams.get("Ftravel");
var Ftype=url.searchParams.get("Ftype");
var Fprice=url.searchParams.get("Fprice");
var date=url.searchParams.get("Fdate");
var passengernum=url.searchParams.get("passengernum");
var travelclass=url.searchParams.get("Ftravelclass");

var checkbox=url.searchParams.get("rcheckbox");
var RFname=url.searchParams.get("rflightname");
var RFid=url.searchParams.get("rflightid");
var RDtime=url.searchParams.get("rdeparturetime");
var RFfrom=url.searchParams.get("rfrom");
var RAtime=url.searchParams.get("rarrivaltime");
var RFto=url.searchParams.get("rto");
var RFtravel=url.searchParams.get("rtravel");
var RFtype=url.searchParams.get("rtype");
var RFprice=url.searchParams.get("rprice");
var Rdate=url.searchParams.get("rdate");




		var email=document.getElementById("emailField").value;
		var firstname=document.getElementById("fname").value;
		var lastname=document.getElementById("lname").value;
		var gender=document.getElementById("gender").value;
		var mobileno=document.getElementById("mobileno").value;	
     
		
		$.ajax({
			type: 'GET',    
			url:'/bin/tcktslingmodel',
			data:'Fname='+Fname+'&Fid='+Fid+'&Dtime='+Dtime+'&Ffrom='+Ffrom+'&Atime='+Atime+'&Fto='+Fto+'&Ftravel='+Ftravel+'&Ftype='+Ftype+'&Fprice='+Fprice+'&travelclass='+travelclass+'&email='+email+'&firstname='+firstname+'&lastname='+lastname+'&gender='+gender+'&mobileno='+mobileno+'&date='+date+'&RFname='+RFname+'&RFid='+RFid+'&RDtime='+RDtime+'&RFfrom='+RFfrom+'&RAtime='+RAtime+'&RFto='+RFto+'&RFtravel='+RFtravel+'&RFtype='+RFtype+'&RFprice='+RFprice+'&Rdate='+Rdate+'&passengernum='+passengernum+'&checkbox='+checkbox,
			success: function(msg){

            	//alert(msg);
            	window.location.replace("/content/classify/en/booking_confirm.html?bookingid="+msg);
			},
			error: function(jqXHR, textStatus, message) {

            	//alert(message);
            window.location.replace("/content/classify/en/booking_confirm.html?bookingid="+message);
			}
		});
	}


    function errormessage(msg) {

    var x = document.getElementById("error");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
    var sucessmsg = msg;
    $("#error").html(sucessmsg);

}
    function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false) 
        {
             errormessage('Invalid Email address');
            return false;
        }
        else{
             var x = document.getElementById("error");
             x.style.display = "none";
        }

        return true;

}
 function validatePhone(mobileno){
        var reg1 = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;

        if (reg1.test(mobileno.value) == false) 
        {
             errormessage('Invalid Phonenumber');
            return false;
        }
        else{
             var x = document.getElementById("error");
             x.style.display = "none";
        }

        return true;

}

