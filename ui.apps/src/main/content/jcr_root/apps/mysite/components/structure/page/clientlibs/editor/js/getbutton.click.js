(function(window, document, $, Granite) {
    "use strict";

    var ERROR_MSG = "An error occurred while processing/fetching the response";

    $(document).on("click", ".page-recommend-button", function(e) {
        var urlParams = new URLSearchParams(window.location.search);
        var pagePathURL = urlParams.get("item");
        $.ajax({
            type: 'GET',
            url: '/travel/reco/pkg.GET_PAGE_DATA.json',
            data: {
                pagePath: pagePathURL
            },
            success: function(data) {
                if (data != "" || data != undefined) {
                    var jsonObj = JSON.parse(data);
                    console.log(jsonObj);
                    if (jsonObj != undefined && jsonObj.Success == true) {
                        var dataToWrite = {}

                        if (jsonObj.PageTitle != undefined) {
                            var pageTitle = jsonObj.PageTitle;
                            $('.page-recommend-text').val(pageTitle);
                            dataToWrite["suggestedpagetitle"] = pageTitle
                        }

                        if( jsonObj.ShortDescription != undefined){
                            var pageDesc = jsonObj.ShortDescription;
                            $("textarea[name='./jcr:description']").val(pageDesc);
							dataToWrite["jcr:description"] = pageDesc
                        }

                        $.ajax({
                                type: 'POST',
                                url: pagePathURL + "/jcr:content",
                                data: dataToWrite
                            })

                        setTimeout(function() { // wait for 2 secs
                            location.reload(); // then reload the page to fetch tags
                        }, 2000);
                    } else {
                        $(window).adaptTo("foundation-ui").alert("ERROR", ERROR_MSG, "error")
                    }
                } else {
                    $(window).adaptTo("foundation-ui").alert("ERROR", ERROR_MSG, "error")
                }
            },
            error: function() {
                $(window).adaptTo("foundation-ui").alert("ERROR", ERROR_MSG, "error")
            }
        });
    });

})(window, document, Granite.$, Granite);