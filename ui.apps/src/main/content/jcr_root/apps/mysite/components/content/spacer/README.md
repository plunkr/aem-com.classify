# Spacer

This component lets you add some vertical space between other components, with an optional horizontal line (equivalent of `hr` tag), eg. to divide sections of a page.

### Properties

- **Size** (`size`): amount of space taken by the component (height)

- **Size (in pixels)** (`sizePx`): same as above, only defined in pixels (requires option 'Custom' to be selected in 'Size' dropdown)

- **Line thickness** (`lineThickness`): thickness of the line (or 'none' to hide the line)

- **Line alignment** (`align`): horizontal alignment of the line inside the component

- **Line direction** (`lineDirection`): horizontal/vertical line

- **Line width** (`lineWidth`): amount of space taken by the line (horizontally, only applies if horizontal direction is selected)
