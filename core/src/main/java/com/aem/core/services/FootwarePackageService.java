package com.aem.core.services;

import org.apache.sling.api.SlingHttpServletRequest;

public interface FootwarePackageService {

	String getFootwareData(SlingHttpServletRequest request);
}
