package com.aem.core.services.impl;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import com.aem.core.service.api.impl.BaseRestRequestImpl;
import com.aem.core.services.FootwarePackageService;
import com.aem.core.utils.CommonUtils;
import com.google.gson.JsonObject;

@Component(service = FootwarePackageService.class)
public class FootwarePackageServiceImpl implements FootwarePackageService {

	private static final String REQUEST_JSON_CONTENT = "Content";

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Override
	public String getFootwareData(SlingHttpServletRequest request) {

		JsonObject requestObject = new JsonObject();

		ResourceResolver resolver = CommonUtils.getResourceResolver(resourceResolverFactory);
		if (resolver == null) {
			resolver = request.getResourceResolver();
		}

		requestObject.addProperty(REQUEST_JSON_CONTENT, "");

		return new BaseRestRequestImpl().getRestWSResponse(requestObject.toString());

	}

}
