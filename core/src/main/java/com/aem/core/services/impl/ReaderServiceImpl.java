package com.aem.core.services.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import com.aem.core.config.ReaderServiceConfig;
import com.aem.core.services.ReaderService;

@Component(service = ReaderService.class, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = ReaderServiceConfig.class)
public class ReaderServiceImpl implements ReaderService {

	@Activate
	private ReaderServiceConfig config;

	private boolean author;
	
	@Reference
	SlingSettingsService settings;


	@Activate
	public void activate(ReaderServiceConfig config) {
		this.config=config;
	}

	@Override
	public String[] getPageData() {
		return config.pageConfigValue();
	}

	public boolean isAuthor() {
		return author;
	}

}
