package com.aem.core.services.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.core.service.api.BaseRestRequest;
import com.aem.core.service.api.impl.BaseRestRequestImpl;
import com.aem.core.services.HolidayPackageService;
import com.aem.core.utils.CommonUtils;
import com.google.gson.JsonObject;

@Component(service = HolidayPackageService.class)
public class HolidayPackageServiceImpl implements HolidayPackageService {

	private static final String CLASS_NAME = HolidayPackageServiceImpl.class.getSimpleName();
	protected final Logger LOG = LoggerFactory.getLogger(this.getClass());
	private String contentDataNode = StringUtils.EMPTY;
	private String pageDataNode = StringUtils.EMPTY;
	private static final String REQUEST_JSON_CONTENT = "Content";	
	private static final String REQUEST_TYPE = "RequestType";
	private static final String REGEX_NEWLINE = "[\\r\\n]+";
	private static final String COMP_CONTENT_DATA = "jcr:description";
	private static final String PAGE_PATH = "pagePath";
	private static final String CONTENT_PATH = "contentPath";
	private static final String CONTENT_TYPE = "contentType";

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	List<Node> childrenList = null;

	public String getHolidayPackage(SlingHttpServletRequest request) throws RepositoryException {
		JsonObject requestObject = new JsonObject();
		ResourceResolver resolver = null;

		if (null != request && null != request.getResourceResolver()) {
			resolver = request.getResourceResolver();
		} else {
			resolver = CommonUtils.getResourceResolver(resourceResolverFactory);
		}

		if (null !=request && null != request.getParameter(CONTENT_PATH)) {
			contentDataNode = request.getParameter(CONTENT_PATH);
			if (null != request.getParameter(PAGE_PATH)) {
				pageDataNode = request.getParameter(PAGE_PATH);
			} else {
				LOG.info("Page Path not found to tag and get the data");
			}
		} else {
			LOG.info("Content Path not found to get the data");
		}

		requestObject.addProperty(REQUEST_TYPE, "ProductDescription");
		requestObject.addProperty(REQUEST_JSON_CONTENT, extractedContentData(resolver));
		BaseRestRequest response = new BaseRestRequestImpl();

		return response.getRestWSResponse(requestObject.toString());

	}

	private String extractedContentData(ResourceResolver resolver) {
		String authoredContentData = StringUtils.EMPTY;
		Resource resource = resolver.getResource(contentDataNode);

		if (resource != null) {
			ValueMap vm = resource.adaptTo(ValueMap.class);
			authoredContentData = vm.get(COMP_CONTENT_DATA).toString();
		}

		authoredContentData = authoredContentData.trim();
		authoredContentData = authoredContentData.replaceAll(REGEX_NEWLINE, StringUtils.EMPTY);
		return authoredContentData;
	}

	private void collectChildList(Node jcrContentNode) {
		try {
			childrenList.add(jcrContentNode);
			if (jcrContentNode.hasNodes()) {
				NodeIterator ni = jcrContentNode.getNodes();
				while (ni.hasNext()) {
					collectChildList(ni.nextNode());
				}
			}
		} catch (RepositoryException e) {
			LOG.info(CLASS_NAME, e.getMessage(), e);
		}
	}

	private String getFullPageData(ResourceResolver resolver) throws RepositoryException {
		String authoredContentData = StringUtils.EMPTY;
		Resource resource = resolver.getResource(pageDataNode + "/jcr:content");
		Node jcrContentNode = resource.adaptTo(Node.class);
		childrenList = new ArrayList<>();

		collectChildList(jcrContentNode);

		if (childrenList != null) {
			Iterator<Node> it = childrenList.iterator();
			while (it.hasNext()) {
				Node childNode = it.next();
				// get only "textimagewithcta" component properties
				if (childNode.getName().contains("textimagewithcta")) {
					authoredContentData += childNode.getProperty(COMP_CONTENT_DATA).getValue().toString() + "###";
				}
			}
		}

		authoredContentData = authoredContentData.trim();
		authoredContentData = authoredContentData.replaceAll(REGEX_NEWLINE, StringUtils.EMPTY);
		return authoredContentData;
	}

	@Override
	public String getPageMetaRecommendation(SlingHttpServletRequest request) throws RepositoryException {
		JsonObject requestObject = new JsonObject();
		ResourceResolver resolver = null;

		if (null != request && null != request.getResourceResolver()) {
			resolver = request.getResourceResolver();
		} else {
			resolver = CommonUtils.getResourceResolver(resourceResolverFactory);
		}

		if (null != request && null != request.getParameter(PAGE_PATH)) {
			pageDataNode = request.getParameter(PAGE_PATH);
		} else {
			LOG.info("Page Path not found to tag and get the data");
		}

		requestObject.addProperty(REQUEST_TYPE, "CategoryPage");
		requestObject.addProperty(REQUEST_JSON_CONTENT, getFullPageData(resolver));
		BaseRestRequest response = new BaseRestRequestImpl();

		return response.getRestWSResponse(requestObject.toString());
	}

}
