package com.aem.core.services.impl;

import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.core.models.reco.BaseResponseModel;
import com.aem.core.models.reco.KeyWords;
import com.aem.core.services.AutoTagGenerationService;
import com.aem.core.utils.CommonUtils;
import com.day.cq.tagging.InvalidTagFormatException;
import com.day.cq.tagging.TagManager;

@Component(service = AutoTagGenerationService.class)
public class AutoTagGenerationServiceImpl implements AutoTagGenerationService {
	protected final static Logger LOG = LoggerFactory.getLogger(AutoTagGenerationServiceImpl.class);
	private String pageDataNode = "/content/classify/en/populardestinations/TravelPage/ContentPage";
	private static final String CQ_TAGS = "cq:tags";
	private static final boolean SET_AUTOSAVE = true;
	private static final String CONTAINER_TAG_ACTIVITY = "Activity";
	private static final String CONTAINER_TAG_LOCATION = "Location";
	private static final String CONTAINER_TAG_HOTELS = "Hotels";
	private static final String CONTAINER_TAG_PACKAGES = "Packages";
	private static final String CONTAINER_TAG_CATEGORY = "Category";
	private static final String CONTAINER_TAG_TARGET = "Target";
	private static final String CONTAINER_TAG_ITEMTYPE = "ItemType";
	private static final String CONTAINER_TAG_BRAND = "Brand";
	private static final String CONTAINER_TAG_OCCASION = "Occasion";
	private static final String CONTAINER_TAG_MATERIAL = "Material";
	private static final String CONTAINER_TAG_KEYS = "Keys";
	private static final String CONTAINER_TAG_WORDS_WITHIN_DESC = "Keys";
	private static final String CONTAINER_TAG_PAGE_TITLE = "PageTitle";
	private static final String CONTAINER_TAG_PAGE_NAV_TITLE = "NavTitle";
	private static final String CONTAINER_TAG_PAGE_SHORT_DESC = "ShortDesc";
	private static final String CONTAINER_TAG_PAGE_CTA_LABEL = "CtaLabel";
	private static final String CONTAINER_TAG_FOOTWARE_PLAYER = "Player";
	private static final String JCR_PATH_SEPARATOR = "/";

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Override
	public String generateAutoTags(SlingHttpServletRequest request, BaseResponseModel baseResponseModel, String requestType) {
		String tagResponse = "ResponsePlaceHolder";

		List<String> tagList = new ArrayList<>();
		Node innerNode = null;
		ResourceResolver resolver = null;

		if (null != request && null != request.getResourceResolver()) {
			resolver = request.getResourceResolver();
		} else {
			resolver = CommonUtils.getResourceResolver(resourceResolverFactory);
		}

		if (null != request.getParameter("pagePath")) {
			pageDataNode = request.getParameter("pagePath");
		} else {
			LOG.info("Taking hardcode page node value from code :" + pageDataNode);
		}

		try {
			final TagManager tagManager = resolver.adaptTo(TagManager.class);

			if (null != tagManager && null != baseResponseModel) {
				if (null != baseResponseModel.getKeywords() && !baseResponseModel.getKeywords().isEmpty()) {
					ListIterator<KeyWords> keywordsIterator = baseResponseModel.getKeywords().listIterator();
					while (keywordsIterator.hasNext()) {
						KeyWords keywords = keywordsIterator.next();
						if (!keywords.getKeys().isEmpty()) {
							List<String> keys = keywords.getKeys();
							ListIterator<String> keysIterator = keys.listIterator();
							while (keysIterator.hasNext()) {
								String keyItem = keysIterator.next();
								tagManager.createTag(CONTAINER_TAG_KEYS + JCR_PATH_SEPARATOR + keyItem, keyItem,
										StringUtils.EMPTY, SET_AUTOSAVE);
								tagList.add(CONTAINER_TAG_KEYS + JCR_PATH_SEPARATOR + keyItem);
							}

						}
						if (StringUtils.isNotBlank(keywords.getWordsWithinDesc())) {
							String areWordsWith = keywords.getWordsWithinDesc();
							tagManager.createTag(CONTAINER_TAG_WORDS_WITHIN_DESC + JCR_PATH_SEPARATOR + areWordsWith,
									areWordsWith, StringUtils.EMPTY, SET_AUTOSAVE);

						}
					}

					if (StringUtils.isNotBlank(baseResponseModel.getCategory())) {
						String category = baseResponseModel.getCategory();
						tagManager.createTag(CONTAINER_TAG_CATEGORY + JCR_PATH_SEPARATOR + category, category,
								StringUtils.EMPTY, SET_AUTOSAVE);
						tagList.add(CONTAINER_TAG_CATEGORY + JCR_PATH_SEPARATOR + category);

					}

					
					if ("CategoryPage".equals(requestType) && StringUtils.isNotBlank(baseResponseModel.getPageTitle())) {
						String pageTitle = baseResponseModel.getPageTitle();
						tagManager.createTag(CONTAINER_TAG_PAGE_TITLE + JCR_PATH_SEPARATOR + pageTitle, pageTitle,
								StringUtils.EMPTY, SET_AUTOSAVE);
						tagList.add(CONTAINER_TAG_PAGE_TITLE + JCR_PATH_SEPARATOR + pageTitle);

					}

					if (StringUtils.isNotBlank(baseResponseModel.getNavigationTitle())) {
						String navTitle = baseResponseModel.getNavigationTitle();
						tagManager.createTag(CONTAINER_TAG_PAGE_NAV_TITLE + JCR_PATH_SEPARATOR + navTitle, navTitle,
								StringUtils.EMPTY, SET_AUTOSAVE);
						tagList.add(CONTAINER_TAG_PAGE_NAV_TITLE + JCR_PATH_SEPARATOR + navTitle);
					}

					if (StringUtils.isNotBlank(baseResponseModel.getShortDesc())) {
						String shortDesc = baseResponseModel.getShortDesc();
						tagManager.createTag(CONTAINER_TAG_PAGE_SHORT_DESC + JCR_PATH_SEPARATOR + shortDesc, shortDesc,
								StringUtils.EMPTY, SET_AUTOSAVE);
						tagList.add(CONTAINER_TAG_PAGE_SHORT_DESC + JCR_PATH_SEPARATOR + shortDesc);

					}

					if (null != baseResponseModel.getCtaLabel() && !baseResponseModel.getCtaLabel().isEmpty()) {
						ListIterator<String> ctaIterator = baseResponseModel.getCtaLabel().listIterator();
						while (ctaIterator.hasNext()) {
							String ctaLabel = ctaIterator.next();
							tagManager.createTag(CONTAINER_TAG_PAGE_CTA_LABEL + JCR_PATH_SEPARATOR + ctaLabel, ctaLabel,
									StringUtils.EMPTY, SET_AUTOSAVE);
							tagList.add(CONTAINER_TAG_PAGE_CTA_LABEL + JCR_PATH_SEPARATOR + ctaLabel);
						}
					}
				}

				generateTagsForTravelHotels(baseResponseModel, tagList, tagManager);

				generateTagsForFootware(baseResponseModel, tagList, tagManager);

				if (!tagList.isEmpty()) {
					String[] pageTagList = tagList.stream().toArray(String[]::new);
					Resource resource = resolver.getResource(pageDataNode);

					if (resource != null) {
						Node node = resource.adaptTo(Node.class);
						if (node != null) {
							NodeIterator nodeIterator = node.getNodes();
							while (nodeIterator.hasNext()) {
								innerNode = nodeIterator.nextNode();
								innerNode.setProperty(CQ_TAGS, pageTagList);
								innerNode.getSession().save();
							}
						}
					}
				}

			}
		} catch (AccessControlException | InvalidTagFormatException | RepositoryException e) {
			LOG.info(e.getMessage(), e);
			e.printStackTrace();
		}
		return tagResponse;

	}

	private void generateTagsForFootware(BaseResponseModel baseResponseModel, List<String> tagList,
			final TagManager tagManager) throws InvalidTagFormatException {
		if (null != baseResponseModel.getFootwareModel()) {
			if (null != baseResponseModel.getFootwareModel().getTarget()
					&& !baseResponseModel.getFootwareModel().getTarget().isEmpty()) {
				ListIterator<String> footwareTarget = baseResponseModel.getFootwareModel().getTarget().listIterator();
				while (footwareTarget.hasNext()) {
					String footwareTargetItem = footwareTarget.next();
					tagManager.createTag(CONTAINER_TAG_TARGET + JCR_PATH_SEPARATOR, footwareTargetItem,
							StringUtils.EMPTY, SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_TARGET + JCR_PATH_SEPARATOR + footwareTargetItem);

				}
			}

			if (null != baseResponseModel.getFootwareModel().getItemType()
					&& !baseResponseModel.getFootwareModel().getItemType().isEmpty()) {
				ListIterator<String> footwareItemType = baseResponseModel.getFootwareModel().getItemType()
						.listIterator();
				while (footwareItemType.hasNext()) {
					String footwareItem = footwareItemType.next();
					tagManager.createTag(CONTAINER_TAG_ITEMTYPE + JCR_PATH_SEPARATOR, footwareItem, StringUtils.EMPTY,
							SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_ITEMTYPE + JCR_PATH_SEPARATOR + footwareItem);

				}
			}

			if (null != baseResponseModel.getFootwareModel().getBrand()
					&& !baseResponseModel.getFootwareModel().getBrand().isEmpty()) {
				ListIterator<String> footwareBrands = baseResponseModel.getFootwareModel().getBrand().listIterator();				
				while (footwareBrands.hasNext()) {
					String footwareBrand = footwareBrands.next();
					tagManager.createTag(CONTAINER_TAG_BRAND + JCR_PATH_SEPARATOR + footwareBrand, footwareBrand,
							StringUtils.EMPTY, SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_BRAND + JCR_PATH_SEPARATOR + footwareBrand);

				}				
			}

			if (StringUtils.isNotBlank(baseResponseModel.getFootwareModel().getOccasion())) {
				String footwareOccasion = baseResponseModel.getFootwareModel().getOccasion();
				tagManager.createTag(CONTAINER_TAG_OCCASION + JCR_PATH_SEPARATOR + footwareOccasion, footwareOccasion,
						StringUtils.EMPTY, SET_AUTOSAVE);
				tagList.add(CONTAINER_TAG_OCCASION + JCR_PATH_SEPARATOR + footwareOccasion);
			}

			if (null != baseResponseModel.getFootwareModel().getMaterial()
					&& !baseResponseModel.getFootwareModel().getMaterial().isEmpty()) {
				ListIterator<String> materialIterator = baseResponseModel.getFootwareModel().getMaterial()
						.listIterator();
				while (materialIterator.hasNext()) {
					String footwareMaterial = materialIterator.next();
					tagManager.createTag(CONTAINER_TAG_MATERIAL + JCR_PATH_SEPARATOR + footwareMaterial,
							footwareMaterial, StringUtils.EMPTY, SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_MATERIAL + JCR_PATH_SEPARATOR + footwareMaterial);
				}
			}

			if (null != baseResponseModel.getFootwareModel().getColor()
					&& !baseResponseModel.getFootwareModel().getItemType().isEmpty()) {
				ListIterator<String> footwareColor = baseResponseModel.getFootwareModel().getColor().listIterator();
				while (footwareColor.hasNext()) {
					String footwareColorItem = footwareColor.next();
					tagManager.createTag(CONTAINER_TAG_ITEMTYPE + JCR_PATH_SEPARATOR, footwareColorItem,
							StringUtils.EMPTY, SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_ITEMTYPE + JCR_PATH_SEPARATOR + footwareColorItem);

				}
			}
			
			if (null != baseResponseModel.getFootwareModel().getPlayer()
					&& !baseResponseModel.getFootwareModel().getPlayer().isEmpty()) {
				ListIterator<String> footwarePlayers = baseResponseModel.getFootwareModel().getPlayer().listIterator();				
				while (footwarePlayers.hasNext()) {
					String footwarePlayer = footwarePlayers.next();
					tagManager.createTag(CONTAINER_TAG_FOOTWARE_PLAYER + JCR_PATH_SEPARATOR + footwarePlayer, footwarePlayer,
							StringUtils.EMPTY, SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_FOOTWARE_PLAYER + JCR_PATH_SEPARATOR + footwarePlayer);

				}				
			}

		}
	}

	private void generateTagsForTravelHotels(BaseResponseModel baseResponseModel, List<String> tagList,
			final TagManager tagManager) throws InvalidTagFormatException {
		if (null != baseResponseModel.getHotelModel()) {
			if (null != baseResponseModel.getHotelModel().getActivities()
					&& !baseResponseModel.getHotelModel().getActivities().isEmpty()) {
				ListIterator<String> activityIterator = baseResponseModel.getHotelModel().getActivities()
						.listIterator();
				while (activityIterator.hasNext()) {
					String activity = activityIterator.next();
					tagManager.createTag(CONTAINER_TAG_ACTIVITY + JCR_PATH_SEPARATOR + activity, activity,
							StringUtils.EMPTY, SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_ACTIVITY + JCR_PATH_SEPARATOR + activity);

				}
			}

			if (baseResponseModel.getHotelModel().getLocation() != null) {
				if (StringUtils.isNotBlank(baseResponseModel.getHotelModel().getLocation().getCity())) {
					String city = baseResponseModel.getHotelModel().getLocation().getCity();
					tagManager.createTag(CONTAINER_TAG_LOCATION + JCR_PATH_SEPARATOR + city, city, StringUtils.EMPTY,
							SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_LOCATION + JCR_PATH_SEPARATOR + city);
				}

				if (StringUtils.isNotBlank(baseResponseModel.getHotelModel().getLocation().getState())) {
					String state = baseResponseModel.getHotelModel().getLocation().getState();
					tagManager.createTag(CONTAINER_TAG_LOCATION + JCR_PATH_SEPARATOR + state, state, StringUtils.EMPTY,
							SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_LOCATION + JCR_PATH_SEPARATOR + state);
				}

				if (StringUtils.isNotBlank(baseResponseModel.getHotelModel().getLocation().getCountry())) {
					String country = baseResponseModel.getHotelModel().getLocation().getCountry();
					tagManager.createTag(CONTAINER_TAG_LOCATION + JCR_PATH_SEPARATOR + country, country,
							StringUtils.EMPTY, SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_LOCATION + JCR_PATH_SEPARATOR + country);
				}

			}

			if (baseResponseModel.getHotelModel().getHotels() != null
					&& !baseResponseModel.getHotelModel().getHotels().isEmpty()) {
				ListIterator<String> hotelsIterator = baseResponseModel.getHotelModel().getHotels().listIterator();
				while (hotelsIterator.hasNext()) {
					String hotel = hotelsIterator.next();
					tagManager.createTag(CONTAINER_TAG_HOTELS + JCR_PATH_SEPARATOR + hotel, hotel, StringUtils.EMPTY,
							SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_HOTELS + JCR_PATH_SEPARATOR + hotel);
				}				
			}

			if (baseResponseModel.getHotelModel().getPackages() != null
					&& !baseResponseModel.getHotelModel().getPackages().isEmpty()) {
				ListIterator<String> packageIterator = baseResponseModel.getHotelModel().getPackages().listIterator();
				while (packageIterator.hasNext()) {
					String packageItem = packageIterator.next();
					tagManager.createTag(CONTAINER_TAG_PACKAGES + JCR_PATH_SEPARATOR, packageItem, StringUtils.EMPTY,
							SET_AUTOSAVE);
					tagList.add(CONTAINER_TAG_PACKAGES + JCR_PATH_SEPARATOR + packageItem);

				}
			}

		}
	}

}
