package com.aem.core.services;

import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;

public interface HolidayPackageService {
	String getHolidayPackage(SlingHttpServletRequest request) throws RepositoryException;
	String getPageMetaRecommendation(SlingHttpServletRequest request) throws RepositoryException;
}
