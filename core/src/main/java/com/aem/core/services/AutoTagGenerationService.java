package com.aem.core.services;

import org.apache.sling.api.SlingHttpServletRequest;

import com.aem.core.models.reco.BaseResponseModel;

public interface AutoTagGenerationService {

	String generateAutoTags(SlingHttpServletRequest request, BaseResponseModel responseJson, String requestType);

}
