package com.aem.core.services;

public interface ReaderService {

	String [] getPageData();
}
