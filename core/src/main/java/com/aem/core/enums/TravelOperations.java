package com.aem.core.enums;

public enum TravelOperations {
	GET_FOOTWARE_DATA,
	GET_HOLIDAY_DATA,
	GET_PAGE_DATA;

	@Override
	public String toString() {
		return name();
	}

}
