package com.aem.core.enums;

public enum HttpMethodTypes {

	GET, POST, PUT, DELETE;

	@Override
	public String toString() {
		return name();
	}

}
