package com.aem.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Page Service Configuration", description = "Service Configuration")
public @interface ReaderServiceConfig {

	@AttributeDefinition(name="Page Config Path", description = "Configuration value")
	String [] pageConfigValue();
}
