package com.aem.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Magento Configuration Console", description = "The configuration for Magento")
public @interface MagentoConfigurationConsole {

	@AttributeDefinition(name = "Magento Admin Username", description = "Username for Magento server Admin Token", type = AttributeType.STRING)
	String getMagentoUsername() default "admin";

	@AttributeDefinition(name = "Magento Admin Password", description = "Password for Magento server Admin Token", type = AttributeType.PASSWORD)
	String getMagentoPassWord() default "";

	@AttributeDefinition(name = "Admin Token endpoint url", description = "Admin Token endpoint url", type = AttributeType.STRING)
	String getAdminTokenUrl() default "";

	@AttributeDefinition(name = "Categories List endpoint url", description = "Categories List endpoint url", type = AttributeType.STRING)
	String getCategoriesListUrl() default "";

	@AttributeDefinition(name = "Category Details endpoint url", description = "Category Details endpoint url", type = AttributeType.STRING)
	String getCategoryDetailsUrl() default "";

	@AttributeDefinition(name = "Category Products endpoint url", description = "Category Products endpoint url", type = AttributeType.STRING)
	String getCategoryProductsUrl() default "";

	@AttributeDefinition(name = "Complete Product Details endpoint url", description = "Complete Product Details endpoint url", type = AttributeType.STRING)
	String getCompleteProductDetailsUrl() default "";

	@AttributeDefinition(name = "Product Search By Category endpoint url", description = "Product Search By Category endpoint url", type = AttributeType.STRING)
	String getProductSearchByCategoryUrl() default "";

	@AttributeDefinition(name = "Create guest cart endpoint url", description = "Create guest cart endpoint url", type = AttributeType.STRING)
	String getCreateGuestCartUrl() default "";

	@AttributeDefinition(name = "Get Cart Details endpoint url", description = "Get Cart Details endpoint url", type = AttributeType.STRING)
	String getGetCartDetailsUrl() default "";

	@AttributeDefinition(name = "Add Product to cart endpoint url", description = "Add Product to cart endpoint url", type = AttributeType.STRING)
	String getAddProducttoCartUrl() default "";

	@AttributeDefinition(name = "Delete Product to cart endpoint url", description = "Delete Product to cart endpoint url", type = AttributeType.STRING)
	String getDeleteProducttoCartUrl() default "";

	@AttributeDefinition(name = "Guest cart Items endpoint url", description = "Guest cart Items endpoint url", type = AttributeType.STRING)
	String getGuestCartItemsUrl() default "";

	@AttributeDefinition(name = "Cart Total endpoint url", description = "Cart Total endpoint url", type = AttributeType.STRING)
	String getCartTotalUrl() default "";

	@AttributeDefinition(name = "Product Variant Prices endpoint url", description = "Product Variant Prices endpoint url", type = AttributeType.STRING)
	String getProductVariantPricesUrl() default "";

	@AttributeDefinition(name = "Product All options endpoint url", description = "Product All options endpoint url", type = AttributeType.STRING)
	String getProductAllOptionsUrl() default "";

	@AttributeDefinition(name = "Get Product variants endpoint url", description = "Get Product variants endpoint url", type = AttributeType.STRING)
	String getGetProductVariantsUrl() default "";

	@AttributeDefinition(name = "Get Billing Address endpoint url", description = "Get Billing Address endpoint url", type = AttributeType.STRING)
	String getBillingAddressUrl() default "";

	@AttributeDefinition(name = "Create Customer endpoint url", description = "Create Customer endpoint url", type = AttributeType.STRING)
	String getCreateCustomerUrl() default "";

	@AttributeDefinition(name = "Create Customer token endpoint url", description = "Create Customer token endpoint url", type = AttributeType.STRING)
	String getUserLoginUrl() default "";

	@AttributeDefinition(name = "Create Customer profile endpoint url", description = "Create Customer profile endpoint url", type = AttributeType.STRING)
	String getCustomerUrl() default "";

}
