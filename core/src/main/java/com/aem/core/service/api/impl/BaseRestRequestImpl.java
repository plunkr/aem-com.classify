package com.aem.core.service.api.impl;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.core.service.api.BaseRestRequest;
import com.aem.core.services.impl.HolidayPackageServiceImpl;

public class BaseRestRequestImpl implements BaseRestRequest {

	private static final String CLASS_NAME = HolidayPackageServiceImpl.class.getSimpleName();
	protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

	private static final int HTTP_SOCKET_TIMEOUT = 30000;
	private static final String END_POINT = "http://13.90.205.53:8080/adobenlp";
	private static final String INTERNAL_RESPONSE_STUB = " { \"Category\": \"Footware\", \"CtaLabel\": [ \"Buy 1 get 1 free\" ], \"Error\": [], \"FootwareAttributes\": { \"Material\": [ \"plastic\" ], \"NonMarking\": false, \"Player\": \"rafa\", \"Sole\": [ \"rubber\" ] }, \"KeyWords\": [ { \"Keys\": [ \"outsole material wraps\", \"black/laser crimson/persian violet/white\", \"unique lacing wraps\", \"medial lace eyestays\", \"lateral side creates\", \"blend durable rubber\", \"medial side\", \"durable rubber\", \"unique laces\", \"forefoot creates\" ], \"WordsWithinDesc\": true } ], \"PageTitle\": \"Nike court air zoom vapor cage - Light Shoes are the right shoes for you.\", \"ShortDescription\": \"The nikecourt air zoom vapor cage 4 is innovated to withstand your toughest matches. inspired by rafa 's playstyle , it 's our most durable shoe to date. unique laces are hidden for extra durability while sliding\", \"Success\": true }";
	private static final String INTERNAL_RESPONSE_STUB_HOTEL = " {\"Category\":\"Hotel\",\"CtaLabel\": [\"Enjoy 2 Nights Free at Orlando\" ],\"Error\": [],\"HotelAttributes\": {\"Activities\": [\"dining\",\"shopping\" ],\"Hotels\":\"Universal's Cabana Bay Beach Resort\",\"Location\": {\"City\":\"OrangeCounty\",\"Country\":\"United States of America\",\"State\":\"Florida\" },\"Offers\": [\"free on booking of more than five days\" ],\"Packages\": [\"3D/2N\" ] },\"KeyWords\": [ {\"Keys\": [\"quick energy boost\",\"40-inch flat-screen tvs\",\"theme parks visit\",\"universal studios florida™\",\"day stay free\",\"free in-room wifi\",\"free wifi\",\"universal citywalk™\",\"stay connected\",\"free toiletries\" ],\"WordsWithinDesc\": true } ],\"PageTitle\":\"Universal Cabana Bay Beach Resort - It’s not a hotel, it’s a way of life.\",\"ShortDescription\":\"Universal's cabana bay beach resort welcomes guests with 2 outdoor pools , 2 restaurants , and free in- room wifi. the rooms have an array of amenities , including ipod docks , safes , and ironing boards. bathrooms offer hair dryers and free toiletries\",\"Success\": true } ";

	@SuppressWarnings("deprecation")
	@Override
	public String getRestWSResponse(String payLoad) {

		String responseObject = StringUtils.EMPTY;

		// initialize data from stub
		responseObject = INTERNAL_RESPONSE_STUB_HOTEL;

		try {
			CloseableHttpResponse response = null;
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(END_POINT);
			HttpEntity stringEntity = new StringEntity(payLoad, ContentType.APPLICATION_JSON);
			httpPost.setEntity(stringEntity);
			response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			responseObject = EntityUtils.toString(entity);
			
		} catch (IOException e) {
			LOG.info(CLASS_NAME, e.getMessage(), e);
		}

		return responseObject;
	}

}
