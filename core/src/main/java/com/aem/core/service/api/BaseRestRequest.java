package com.aem.core.service.api;

public interface BaseRestRequest {

	String getRestWSResponse(String request);
	
}
