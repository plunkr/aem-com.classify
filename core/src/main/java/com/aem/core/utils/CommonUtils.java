package com.aem.core.utils;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.jcr.Value;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.value.StringValue;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CommonUtils {
	protected final static Logger LOG = LoggerFactory.getLogger(CommonUtils.class);
	private static Gson gsonInstance = new Gson();
	private static final int MAX_SIZE_OF_POST_REQUEST = 1024 * 16;

	private static final String SUBSERVICE = "datawrite";
 
	private CommonUtils() {
	}

	public static JsonObject getJsonObjectFromString(final String data) {
		JsonObject jsonObject = null;
		if (StringUtils.isNotBlank(data)) {
			final JsonElement element = getGson().fromJson(data, JsonElement.class);
			if (element != null) {
				jsonObject = element.getAsJsonObject();
			}
		}
		return jsonObject;
	}

	public static Gson getGson() {
		if (gsonInstance == null) {
			throw new IllegalStateException("Failed to create Gson instance for Singleton");
		}
		return gsonInstance;
	}

	public static JsonObject extractDataFromRequestJson(SlingHttpServletRequest request) {
		JsonObject jsonData = new JsonObject();
		if (request != null && request.getContentLength() > 0
				&& request.getContentLength() < MAX_SIZE_OF_POST_REQUEST) {
			StringBuilder payload = new StringBuilder();
			String line = null;
			BufferedReader bufferedReader = null;
			try {
				bufferedReader = request.getReader();
				while ((line = bufferedReader.readLine()) != null) {
					payload.append(line);
				}
				if (payload.length() > 0) {
					String data = payload.toString();
					if (StringUtils.isNotBlank(data)) {
						jsonData = CommonUtils.getJsonObjectFromString(data);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			if (request == null) {
				LOG.error("for extractDataFromRequestJson: Request is null");
			} else if (request.getContentLength() == 0) {
				LOG.info("No Json exist");
			} else if (!StringUtils.contains(request.getContentType(), "application/json")) {
				LOG.info("Wrong Content Type/Json Mismatch:" + request.getContentType());
			}
		}
		return jsonData;
	}

	public static ResourceResolver getResourceResolver(final ResourceResolverFactory resolverFactory) {
		String methodName = "getResourceResolver";
		ResourceResolver resourceResolver = null;
		try {
			if (resolverFactory != null) {
				Map<String, Object> param = new HashMap<>();
				param.put(ResourceResolverFactory.SUBSERVICE, SUBSERVICE);
				resourceResolver = resolverFactory.getServiceResourceResolver(param);
			} else {
				LOG.error("Factory is null.", methodName);
			}

		} catch (LoginException e) {
			e.printStackTrace();
		}
		return resourceResolver;
	}
	
	private static Value[] convertListToJcrValue(List<String> list) {
		Value[] values = new Value[list.size()];
		for (int i = 0; i < list.size(); i++) {
			values[i] = new StringValue(list.get(i));
		}
		return values;
	}
	

}
