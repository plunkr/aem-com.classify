package com.aem.core.constant;


public class TravelConstant {
	//Error Constant 
    public static final String HTTP_EXCEPTION = "HTTP_EXCEPTION";
    public static final String CONNECT_EXCEPTION = "CONNECT_EXCEPTION";
    public static final String SOCKET_TIMEOUT_EXCEPTION = "SOCKET_TIMEOUT_EXCEPTION";
    public static final String CONNECTIONPOOL_TIMEOUT_EXCEPTION ="CONNECTIONPOOL_TIMEOUT_EXCEPTION";
    public static final String CONNECT_TIMEOUT_EXCEPTION = "CONNECT_TIMEOUT_EXCEPTION";
    public static final String IO_EXCEPTION = "IO_EXCEPTION";
    public static final String ILLEGALARGUMENT_EXCEPTION = "ILLEGALARGUMENT_EXCEPTION";
    public static final String EXCEPTION ="EXCEPTION";
    public static final String ERROR_CLOSING_CONNECTION = "ERROR_CLOSING_CONNECTION";
    
  //property  Constant  
    public static final String client_id = "vgc6yx2mxmzfhccmvms6agyd";
    public static final String client_secret = "T4MCrg8eQh";
    public static final String client_url = "https://api.com/v1/oauth/token";
    public static final String grant_type = "client_credentials";
    public static final String client_partnerurl = "https://developer.com/io-docs/getoauth2accesstoken?apiId=17041&auth_flow=client_cred&";
}