package com.aem.core.constant;

public class ParameterConstants {
	private ParameterConstants() {
	}

	public static final String ACTIVITIES = "Activities";
	public static final String ACTIVITY_LIST = "ActivitiesList";

	public static final String HOTELS = "Hotels";
	public static final String OFFERS = "Offers";
	public static final String LOCATION = "Location";
	public static final String PACKAGES = "Packages";
	public static final String CITY = "City";
	public static final String STATE = "State";
	public static final String COUNTRY = "Country";
	
	
	public static final String FOOTWARE_ATTRIBUTES="FootwareAttributes";
	public static final String TARGET="Target";
	public static final String ITEM_TYPE="ItemType";
	public static final String BRAND="Brand";
	public static final String OCCASION="Occasion";
	public static final String MATERIALS="Material";
	public static final String COLOR="Color";
	public static final String KEYWORDS="KeyWords";
	public static final String KEYS="Keys";
	public static final String WORDS_WITHIN_DESC="WordsWithinDesc";
	public static final String PAGE_TITLE="PageTitle";
	public static final String NAV_TITILE="NavTitle";
	public static final String SHORT_DESC="ShortDesc";
	public static final String CTA_LABEL="CtaLabel";
	public static final String HOTEL_ATTRIBUTES="HotelAttributes";
	public static final String CATEGORY = "Category";
	public static final String FOOTWARE = "FootwareAttributes";
	public static final String ERROR = "Error";
	public static final String SUCCESS = "Success";
	public static final String PLAYER = "Player";
	public static final String IS_NON_MARKETING = "NonMarking";



	

	

	
	
}
