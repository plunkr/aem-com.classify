package com.aem.core.models.reco;

import java.util.ArrayList;
import java.util.List;
import com.aem.core.constant.ParameterConstants;
import com.google.gson.annotations.SerializedName;

public class KeyWords {

	@SerializedName(ParameterConstants.KEYS)
	private List<String> keys = new ArrayList<>();

	@SerializedName(ParameterConstants.WORDS_WITHIN_DESC)
	private String wordsWithinDesc;

	public List<String> getKeys() {
		return keys;
	}

	public void setKeys(List<String> keys) {
		this.keys = keys;
	}

	public String getWordsWithinDesc() {
		return wordsWithinDesc;
	}

	public void setWordsWithinDesc(String wordsWithinDesc) {
		this.wordsWithinDesc = wordsWithinDesc;
	}

}
