package com.aem.core.models.reco;

import java.util.ArrayList;
import java.util.List;
import com.aem.core.constant.ParameterConstants;
import com.google.gson.annotations.SerializedName;

public class FootwareModel {

	@SerializedName(ParameterConstants.TARGET)
	private List<String> target = new ArrayList<>();

	@SerializedName(ParameterConstants.ITEM_TYPE)
	private List<String> itemType = new ArrayList<>();

	@SerializedName(ParameterConstants.BRAND)
	private List<String> brand;

	@SerializedName(ParameterConstants.OCCASION)
	private String occasion;

	@SerializedName(ParameterConstants.MATERIALS)
	private List<String> material = new ArrayList<>();

	@SerializedName(ParameterConstants.COLOR)
	private List<String> color = new ArrayList<>();

	@SerializedName(ParameterConstants.PLAYER)
	private List<String> player;

	@SerializedName(ParameterConstants.IS_NON_MARKETING)
	private String isNonMarketing;

	public List<String> getTarget() {
		return target;
	}

	public void setTarget(List<String> target) {
		this.target = target;
	}

	public List<String> getItemType() {
		return itemType;
	}

	public void setItemType(List<String> itemType) {
		this.itemType = itemType;
	}

	public List<String> getBrand() {
		return brand;
	}

	public void setBrand(List<String> brand) {
		this.brand = brand;
	}

	public String getOccasion() {
		return occasion;
	}

	public void setOccasion(String occasion) {
		this.occasion = occasion;
	}

	public List<String> getMaterial() {
		return material;
	}

	public void setMaterial(List<String> material) {
		this.material = material;
	}

	public List<String> getColor() {
		return color;
	}

	public void setColor(List<String> color) {
		this.color = color;
	}

	public List<String> getPlayer() {
		return player;
	}

	public void setPlayer(List<String> player) {
		this.player = player;
	}

	public String getIsNonMarketing() {
		return isNonMarketing;
	}

	public void setIsNonMarketing(String isNonMarketing) {
		this.isNonMarketing = isNonMarketing;
	}

}
