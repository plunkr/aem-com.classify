package com.aem.core.models.reco;

import java.util.ArrayList;
import java.util.List;
import com.aem.core.constant.ParameterConstants;
import com.google.gson.annotations.SerializedName;

public class BaseResponseModel {

	@SerializedName(ParameterConstants.CATEGORY)
	private String category;

	@SerializedName(ParameterConstants.HOTEL_ATTRIBUTES)
	private HotelModel hotelModel;

	@SerializedName(ParameterConstants.FOOTWARE_ATTRIBUTES)
	private FootwareModel footwareModel;

	@SerializedName(ParameterConstants.KEYWORDS)
	private List<KeyWords> keywords;

	@SerializedName(ParameterConstants.PAGE_TITLE)
	private String pageTitle;

	@SerializedName(ParameterConstants.NAV_TITILE)
	private String navigationTitle;

	@SerializedName(ParameterConstants.SHORT_DESC)
	private String shortDesc;

	@SerializedName(ParameterConstants.CTA_LABEL)
	private List<String> ctaLabel = new ArrayList<>();

	@SerializedName(ParameterConstants.ERROR)
	private List<String> error = new ArrayList<>();

	@SerializedName(ParameterConstants.SUCCESS)
	private boolean success;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public HotelModel getHotelModel() {
		return hotelModel;
	}

	public void setHotelModel(HotelModel hotelModel) {
		this.hotelModel = hotelModel;
	}

	public FootwareModel getFootwareModel() {
		return footwareModel;
	}

	public void setFootwareModel(FootwareModel footwareModel) {
		this.footwareModel = footwareModel;
	}

	public List<KeyWords> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<KeyWords> keywords) {
		this.keywords = keywords;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getNavigationTitle() {
		return navigationTitle;
	}

	public void setNavigationTitle(String navigationTitle) {
		this.navigationTitle = navigationTitle;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}


	public List<String> getCtaLabel() {
		return ctaLabel;
	}

	public void setCtaLabel(List<String> ctaLabel) {
		this.ctaLabel = ctaLabel;
	}

	public List<String> getError() {
		return error;
	}

	public void setError(List<String> error) {
		this.error = error;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return "BaseResponseModel [category=" + category + ",hotelModel=" + hotelModel + ", footwareModel="
				+ footwareModel + ",keywords" + keywords +",footwareModel"+ footwareModel+"]";
	}

}
