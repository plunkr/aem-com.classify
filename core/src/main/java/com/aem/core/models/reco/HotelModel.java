package com.aem.core.models.reco;

import java.util.ArrayList;
import java.util.List;
import com.aem.core.constant.ParameterConstants;
import com.google.gson.annotations.SerializedName;

public class HotelModel {

	@SerializedName(ParameterConstants.LOCATION)
	private LocationModel location;

	@SerializedName(ParameterConstants.PACKAGES)
	private List<String> packages = new ArrayList<>();

	@SerializedName(ParameterConstants.HOTELS)
	private List<String> hotels;

	@SerializedName(ParameterConstants.OFFERS)
	private List<String> offers = new ArrayList<>();

	@SerializedName(ParameterConstants.ACTIVITIES)
	private List<String> activities = new ArrayList<>();
	
	public List<String> getActivities() {
		return activities;
	}

	public void setActivities(List<String> activities) {
		this.activities = activities;
	}

	public List<String> getHotels() {
		return hotels;
	}

	public void setHotels(List<String> hotels) {
		this.hotels = hotels;
	}

	public LocationModel getLocation() {
		return location;
	}

	public void setLocation(LocationModel location) {
		this.location = location;
	}

	public List<String> getOffers() {
		return offers;
	}

	public void setOffers(List<String> offers) {
		this.offers = offers;
	}

	public List<String> getPackages() {
		return packages;
	}

	public void setPackages(List<String> packages) {
		this.packages = packages;
	}

}
