package com.aem.core.models.reco;

import com.aem.core.constant.ParameterConstants;
import com.google.gson.annotations.SerializedName;

public class LocationModel {

	@SerializedName(ParameterConstants.CITY)
	private String city;

	@SerializedName(ParameterConstants.STATE)
	private String state;

	@SerializedName(ParameterConstants.COUNTRY)
	private String country;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Location [city=" + city + ",state=" + state + ", country=" + country + "]";
	}

}
