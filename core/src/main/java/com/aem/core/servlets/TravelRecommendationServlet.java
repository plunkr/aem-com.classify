package com.aem.core.servlets;

import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_METHODS;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_PATHS;
import java.io.IOException;
import javax.servlet.ServletException;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.core.enums.TravelOperations;
import com.aem.core.models.reco.BaseResponseModel;
import com.aem.core.services.AutoTagGenerationService;
import com.aem.core.services.FootwarePackageService;
import com.aem.core.services.HolidayPackageService;
import com.aem.core.utils.CommonUtils;
import org.osgi.service.component.annotations.Component;

@Component(service = Servlet.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=Travel Recomendation Servlet", SLING_SERVLET_PATHS + "=/travel/reco/pkg",
		SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET })
public class TravelRecommendationServlet extends BaseRecommendationServlet {

	private static final long serialVersionUID = -9087401611180331509L;
	private static final Logger LOG = LoggerFactory.getLogger(TravelRecommendationServlet.class);

	@Reference
	private HolidayPackageService holidayPackageService;

	@Reference
	private AutoTagGenerationService autoTagGenerationService;

	@Reference
	private FootwarePackageService footwarePackageService;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) {
		String jsonResponse = StringUtils.EMPTY;
		BaseResponseModel baseResponse = null;

		TravelOperations travelOperations = null;
		if (request.getRequestPathInfo() != null && request.getRequestPathInfo().getSelectorString() != null) {
			travelOperations = getOperations(request, travelOperations);
		} else {
			LOG.error("Selector Not Found in the request");
		}
		try {
			if (travelOperations != null) {
				switch (travelOperations) {
				case GET_HOLIDAY_DATA:
					jsonResponse = holidayPackageService.getHolidayPackage(request);
					baseResponse = CommonUtils.getGson().fromJson(jsonResponse, BaseResponseModel.class);
					autoTagGenerationService.generateAutoTags(request, baseResponse, StringUtils.EMPTY);
					break;
				case GET_PAGE_DATA:
					jsonResponse = holidayPackageService.getPageMetaRecommendation(request);
					baseResponse = CommonUtils.getGson().fromJson(jsonResponse, BaseResponseModel.class);
					autoTagGenerationService.generateAutoTags(request, baseResponse, "CategoryPage");
					break;
				case GET_FOOTWARE_DATA:
					footwarePackageService.getFootwareData(request);
					break;
				default:
					LOG.info("No switch case found !" + travelOperations, "doPost in doGet", LOG);
					break;
				}
			} else {
				StringBuilder logger = new StringBuilder();
				logger.append("Operation parameter missing or incorrect:");
				LOG.info(logger.toString(),jsonResponse);
			}
			processResponse(response, CommonUtils.getGson().toJson(jsonResponse));
		} catch (IOException | RepositoryException e) {
			LOG.info(e.getMessage(), e, LOG);
			e.printStackTrace();
		}

	}

	private TravelOperations getOperations(final SlingHttpServletRequest request, TravelOperations travelOperations) {
		TravelOperations travelOps = travelOperations;
		if (request.getRequestPathInfo().getSelectorString().contains(TravelOperations.GET_HOLIDAY_DATA.toString())) {
			travelOps = TravelOperations.GET_HOLIDAY_DATA;
		}

		if (request.getRequestPathInfo().getSelectorString().contains(TravelOperations.GET_PAGE_DATA.toString())) {
			travelOps = TravelOperations.GET_PAGE_DATA;
		}

		if (request.getRequestPathInfo().getSelectorString().contains(TravelOperations.GET_FOOTWARE_DATA.toString())) {
			travelOps = TravelOperations.GET_FOOTWARE_DATA;
		}
		return travelOps;
	}

}