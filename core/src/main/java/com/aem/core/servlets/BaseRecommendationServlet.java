package com.aem.core.servlets;

import java.io.IOException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.xss.XSSAPI;

public class BaseRecommendationServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = -5777521728077601892L;
	public static final String X_CONTENT_TYPE_OPTIONS = "X-Content-Type-Options";

	protected void processResponse(final SlingHttpServletResponse response, final String jsonResponse)
			throws IOException {
		if (!response.containsHeader(X_CONTENT_TYPE_OPTIONS)) {
			response.addHeader(X_CONTENT_TYPE_OPTIONS, "nosniff");
		}
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonResponse);
	}

	protected XSSAPI getXSSAPI(final SlingHttpServletRequest slingRequest) {
		return slingRequest.getResourceResolver().adaptTo(XSSAPI.class);
	}

}
