package com.aem.core.helpers;

public final class CommonConstants {

    public static final int CONNECTION_TIMEOUT = 15000;
    public static final int SO_TIMEOUT = 15000;
    public static final int MAX_TOTAL_CONNECTIONS = 5000;
    public static final int DEFAULT_MAX_CONNECTIONS_PER_HOST = 5000;

    private CommonConstants() {
    }
}
