package com.aem.core.helpers;

import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by apple on 28/04/17.
 */
public final class HttpMultiThreadedConMgr {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpMultiThreadedConMgr.class);
    private static MultiThreadedHttpConnectionManager conmgr;
    private HttpMultiThreadedConMgr() {
    }
    public static MultiThreadedHttpConnectionManager getMultiThreadedConMgr() {
        LOGGER.info("Returning the connection manager instance");
        if (conmgr == null) {
            conmgr = new MultiThreadedHttpConnectionManager();
            conmgr.getParams().setConnectionTimeout(CommonConstants.CONNECTION_TIMEOUT);
            conmgr.getParams().setSoTimeout(CommonConstants.SO_TIMEOUT);
            conmgr.getParams().setMaxTotalConnections(CommonConstants.MAX_TOTAL_CONNECTIONS);
            conmgr.getParams().setDefaultMaxConnectionsPerHost(CommonConstants.DEFAULT_MAX_CONNECTIONS_PER_HOST);
        }
        return conmgr;
    }
}
